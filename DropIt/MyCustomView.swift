//
//  MyCustomView.swift
//  Test
//
//  Created by Yijia Huang on 8/27/17.
//  Copyright © 2017 Yijia Huang. All rights reserved.
//

import UIKit

@IBDesignable
class MyCustomView: UIView {
    
    
    
    var skullRadius: CGFloat {
        return min(bounds.size.width, bounds.size.height) / 2
    }
    
    var skullCenter: CGPoint {
        return CGPoint(x: bounds.midX, y: bounds.midY)
    }
    
    func pathForSkull() -> UIBezierPath {
        let path = UIBezierPath()
        path.append(UIBezierPath(rect: CGRect(x: skullCenter.x, y: skullCenter.y, width: 20, height: 20)))
        return path
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        // Create a CAShapeLayer
//        let slp1 = UIBezierPath()
//        let sl1 = CAShapeLayer()
//        slp1.append(path1())
//        slp1.append(path2())
//        slp1.append(path3())
//        slp1.append(path4())
//        sl1.path = slp1.cgPath
//        sl1.fillColor = UIColor.red.cgColor
//        sl1.lineWidth = 1.0
//        sl1.position = CGPoint(x: 5, y: 5)
//        self.layer.addSublayer(sl1)
//        
//        let slp2 = UIBezierPath()
//        let sl2 = CAShapeLayer()
//        slp2.append(path4())
//        sl2.path = slp2.cgPath
//        sl2.fillColor = UIColor.blue.cgColor
//        sl2.lineWidth = 1.0
//        sl2.position = CGPoint(x: 5, y: 30)
//        self.layer.addSublayer(sl2)
        
        
//        let ShapeLayerRD = CAShapeLayer()
//        ShapeLayerRD.frame = CGRect(center: skullCenter, size: <#T##CGSize#>)
//        ShapeLayerRD.bounds = CGRect(center: <#T##CGPoint#>, size: <#T##CGSize#>)
//        ShapeLayerRD.path = pathForSkull().cgPath
//        ShapeLayerRD.fillColor = UIColor.yellow.cgColor
//        ShapeLayerRD.lineWidth = 1.0
//        ShapeLayerRD.position = CGPoint(x: 0, y: 0)
//        self.layer.addSublayer(ShapeLayerRD)
    }
    
    func path1() -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 2, y: 26))
        path.addLine(to: CGPoint(x: 2, y: 15))
        path.addCurve(to: CGPoint(x: 0, y: 12), // ending point
            controlPoint1: CGPoint(x: 2, y: 14),
            controlPoint2: CGPoint(x: 0, y: 14))
        path.addLine(to: CGPoint(x: 0, y: 2))
        path.addArc(withCenter: CGPoint(x: 2, y: 2), // center point of circle
            radius: 2, // this will make it meet our path line
            startAngle: CGFloat(Double.pi), // π radians = 180 degrees = straight left
            endAngle: CGFloat(3/2 * Double.pi), // 3π/2 radians = 270 degrees = straight up
            clockwise: true)
        path.addLine(to: CGPoint(x: 8, y: 0))
        path.addArc(withCenter: CGPoint(x: 8, y: 2),
                    radius: 2,
                    startAngle: CGFloat(3/2 * Double.pi), // straight up
            endAngle: CGFloat(0), // 0 radians = straight right
            clockwise: true)
        path.addLine(to: CGPoint(x: 10, y: 12))
        path.addCurve(to: CGPoint(x: 8, y: 15), // ending point
            controlPoint1: CGPoint(x: 10, y: 14),
            controlPoint2: CGPoint(x: 8, y: 14))
        path.addLine(to: CGPoint(x: 8, y: 26))
        path.close()
        return path
    }
    
    func path2() -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 20, y: 5))
        path.addLine(to: CGPoint(x: 30, y: 5))
        path.addLine(to: CGPoint(x: 30, y: 15))
        path.close()
        return path
    }
    
    func path3() -> UIBezierPath {
        let path = UIBezierPath()
        path.append(UIBezierPath(rect: CGRect(x: 40, y: 5, width: 20, height: 20)))
        return path
    }
    
    func path4() -> UIBezierPath {
        let path = UIBezierPath()
        path.append(UIBezierPath(ovalIn: CGRect(x: 70, y: 5, width: 20, height: 20)))
        return path
    }
    
    //My Ring
    var myRingCenter: CGPoint {
        return CGPoint(x: bounds.midX, y: bounds.midY)
    }
    
    var scale = CGFloat(1.0)
    
    var myRingHeight: CGFloat {
        return myRingRadius / 5
    }
    var myRingRadius: CGFloat {
        return bounds.width / 2 * scale
    }
    
    func myRightDownQuarterRing() -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: myRingCenter.x, y: myRingCenter.y + myRingRadius))
        path.addLine(to: CGPoint(x: myRingCenter.x, y: myRingCenter.y + myRingRadius - myRingHeight))
        path.addArc(withCenter: myRingCenter, radius: myRingRadius - myRingHeight, startAngle: CGFloat.pi / 2, endAngle: CGFloat(0), clockwise:   false)
        path.addLine(to: CGPoint(x: myRingCenter.x + myRingRadius, y: myRingCenter.y))
        path.addArc(withCenter: myRingCenter, radius: myRingRadius, startAngle: CGFloat(0), endAngle: CGFloat.pi / 2, clockwise: true)
        return path
    }
    
    func myLeftDownQuarterRing() -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: myRingCenter.x, y: myRingCenter.y + myRingRadius))
        path.addLine(to: CGPoint(x: myRingCenter.x, y: myRingCenter.y + myRingRadius - myRingHeight))
        path.addArc(withCenter: myRingCenter, radius: myRingRadius - myRingHeight, startAngle: CGFloat.pi / 2, endAngle: CGFloat.pi, clockwise:   true)
        path.addLine(to: CGPoint(x: myRingCenter.x - myRingRadius, y: myRingCenter.y))
        path.addArc(withCenter: myRingCenter, radius: myRingRadius, startAngle: CGFloat.pi, endAngle: CGFloat.pi / 2, clockwise: false)
        return path
    }
    
    func myLeftUpQuarterRing() -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: myRingCenter.x - myRingRadius, y: myRingCenter.y))
        path.addLine(to: CGPoint(x: myRingCenter.x - myRingRadius + myRingHeight, y: myRingCenter.y))
        path.addArc(withCenter: myRingCenter, radius: myRingRadius - myRingHeight, startAngle: CGFloat.pi, endAngle: CGFloat.pi * 3 / 2, clockwise:   true)
        path.addLine(to: CGPoint(x: myRingCenter.x, y: myRingCenter.y - myRingRadius))
        path.addArc(withCenter: myRingCenter, radius: myRingRadius, startAngle: CGFloat.pi * 3 / 2, endAngle: CGFloat.pi, clockwise: false)
        return path
    }

    func myRightUpQuarterRing() -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: myRingCenter.x, y: myRingCenter.y - myRingRadius))
        path.addLine(to: CGPoint(x: myRingCenter.x, y: myRingCenter.y - myRingRadius + myRingHeight))
        path.addArc(withCenter: myRingCenter, radius: myRingRadius - myRingHeight, startAngle: CGFloat.pi * 3 / 2, endAngle: CGFloat(0), clockwise:   true)
        path.addLine(to: CGPoint(x: myRingCenter.x + myRingRadius, y: myRingCenter.y))
        path.addArc(withCenter: myRingCenter, radius: myRingRadius, startAngle: CGFloat(0), endAngle: CGFloat.pi * 3 / 2, clockwise: false)
        return path
    }
    
    override func draw(_ rect: CGRect) {
        let color = UIColor.random
        var path = myRightDownQuarterRing()
        color.setFill()
        path.fill()
        
        path = myLeftDownQuarterRing()
        color.setFill()
        path.fill()
        
        path = myLeftUpQuarterRing()
        color.setFill()
        path.fill()
        
        path = myRightUpQuarterRing()
        color.setFill()
        path.fill()
    }

}
