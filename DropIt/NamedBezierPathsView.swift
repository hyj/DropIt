//
//  NamedBezierPathsView.swift
//  DropIt
//
//  Created by Yijia Huang on 8/26/17.
//  Copyright © 2017 Yijia Huang. All rights reserved.
//

import UIKit

class NamedBezierPathsView: UIView {

    var bezierPaths = [String:UIBezierPath]() { didSet { setNeedsDisplay() } }
    
    override func draw(_ rect: CGRect) {
        UIColor.blue.set()
        for (_, path) in bezierPaths {
            path.stroke()
        }
    }
}
